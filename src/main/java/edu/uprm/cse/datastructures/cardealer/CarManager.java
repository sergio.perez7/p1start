package edu.uprm.cse.datastructures.cardealer;

import java.util.Iterator;
import java.util.Optional;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;


@Path("/cars")
public class CarManager {
	
	@GET @Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		
		if(CarList.getInstance().isEmpty())
			return null;
		return CarList.getCarArray();
	}

	@GET @Path("{id}") @Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") int id) {
		for (Iterator<Car> i = CarList.getInstance().iterator(); i.hasNext();) {
			Car c = i.next();
			if(c.getCarId() == id) {
				System.out.println("GET: " + c);
				return c;
			}
		}
		System.out.println("Car not found in getCar().");
		throw new NotFoundException("Error Car " + id + " not found");
	}
	
	@POST @Path("/add") @Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		CarList.getInstance().add(car);
		System.out.println("POST: " + car);
		return Response.status(Response.Status.CREATED).build();
	}

	@PUT @Path("{id}/update") @Produces(MediaType.APPLICATION_JSON)
	public Response update(Car car) {
		CarList.getInstance().remove(getCar((int)car.getCarId()));
		CarList.getInstance().add(car);
		return Response.status(Response.Status.OK).build();
	}
	
	@DELETE @Path("{id}/delete")
	public Response delete(@PathParam("id") int id) {
		int count = 0;
		for (Iterator<Car> i = CarList.getInstance().iterator(); i.hasNext();) {
			Car c = i.next();
			if(c.getCarId() == id) {
				CarList.getInstance().remove(count);
				return Response.status(Response.Status.OK).build();
			}
			count++;
		}
		System.out.println("Car not found in delete().");
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
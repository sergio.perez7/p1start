package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	
	private static CircularSortedDoublyLinkedList<Car> list = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	public CarList() {}
	
	public static CircularSortedDoublyLinkedList<Car> getInstance()	{ return list; }
	
	public static Car[] getCarArray() {

		Car[] cars = new Car[list.size()];
		
		for (int i = 0; i < list.size(); i++) {
			cars[i] = list.get(i);
		}
		
		return cars;
	}
	
	public static void resetCars() { CarList.getInstance().clear(); }
}
